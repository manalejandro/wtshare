# wtshare - WebTorrent Share

You can share files from the browser with P2P technology ([WebTorrents](https://webtorrent.io/)) without installing anything, there is no size limit

![wtshare](capture.png)

This project can be used with NodeJS only, but if you like you can use `docker` and `docker-compose` to deploy

## Configuration and Recommendations

- You need to set some variables in the `public/js/main.js` file to work properly
- `Warning`: be careful if you put this project into production, you are using a public local torrent tracker, look at the [bittorrent-tracker](https://github.com/webtorrent/bittorrent-tracker) options to filter connections, consider using it behind a proxy

## Build

```bash
docker-compose build --no-cache
```

## Run

```bash
docker-compose up -d
```

## License

MIT
