const finalhandler = require('finalhandler'),
    http = require('http'),
    serveStatic = require('serve-static'),
    serve = serveStatic('public/', { 'index': ['index.html'] }),
    server = http.createServer(onRequest = (req, res) => {
        serve(req, res, finalhandler(req, res))
    }).listen(3000, () => {
        console.log(`Listening on: ${server.address().address}:${server.address().port}`)
    })
