FROM node:16-buster-slim
RUN npm i bittorrent-tracker -g
COPY --chown=node:node . /home/node/wtshare
USER node
WORKDIR /home/node/wtshare
RUN npm i
ENTRYPOINT ["/bin/bash", "/home/node/wtshare/entrypoint.sh"]
