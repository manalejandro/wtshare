var downloadTorrent = function (torrent) {
    saveAs(torrent.torrentFileBlobURL, torrent.name + '.torrent')
}
$(document).ready(function () {
    var trackers = [
        'ws://0.0.0.0:8888',
        'http://0.0.0.0:8888/announce'
    ],
        iceServers = [
            { urls: ['turn:???.com', 'stun:???.com'], username: '???', credential: '???' }
        ],
        holder = $('.holder'),
        input = $('.input'),
        file = $('.file')
    function torrentEvents(torrent, seeding) {
        torrent.on('metadata', function () {
            $('#title').text(torrent.name)
            $('#hash').text('/#' + torrent.infoHash)
            $('#hash').attr('href', '/#' + torrent.infoHash)
            $('#size').text(Math.round((torrent.length / 1024 / 1024) * 100) / 100)
        })
        torrent.on('ready', function () {
            $('#holder').empty()
            window.torrent = torrent
        })
        torrent.on('download', function (bytes) {
            $('#status').text('Downloading')
            updateTorrent(torrent)
            $('#progress').text(Math.round(torrent.progress * 10000) / 100 || '0')
        })
        torrent.on('wire', function (wire) {
            $('#peers').text(torrent.numPeers || '0')
        })
        torrent.on('done', function () {
            $('#status').text('Downloaded')
            torrent.files.forEach(function (file) {
                file.appendTo('#holder', { autoplay: true, mute: true }, function (err, elem) {
                    if (err) {
                        $('#status').text(err)
                    } else {
                        $('#status').text('Content loaded')
                    }
                })
            })
            updateTorrent(torrent)
            $('#downloading').text('0')
            $('#uploading').text('0')
            if (!seeding) {
                torrent.files.forEach(function (data) {
                    data.getBlobURL(function (err, url) {
                        saveAs(url, data.name)
                    })
                })
            }
        })
        torrent.on('upload', function (data) {
            $('#status').text('Uploading')
            updateTorrent(torrent)
        })
        torrent.on('noPeers', function () {
            $('#status').text('No Peers')
            updateTorrent(torrent)
        })
    }
    function seed(files) {
        var client = new WebTorrent({
                tracker: WebTorrent.WEBRTC_SUPPORT ? {
                    rtcConfig: {
                        iceServers: iceServers
                    }
                } : false
            }),
            torrent = client.seed(files, {
            announce: trackers
        })
        torrentEvents(torrent, true)
    }
    function updateTorrent(torrent) {
        $('#downloading').text(Math.round(torrent.downloadSpeed / 10) / 100 || '0')
        $('#downloaded').text(Math.round((torrent.downloaded / 1024 / 1024) * 100) / 100)
        $('#uploading').text(Math.round(torrent.uploadSpeed / 10) / 100 || '0')
        $('#uploaded').text(Math.round((torrent.uploaded / 1024 / 1024) * 100) / 100)
        $('#peers').text(torrent.numPeers || 0)
    }
    holder.on('drop', function (event) {
        event.preventDefault()
        event.stopPropagation()
        holder.removeClass('white-border')
        if (event.originalEvent.dataTransfer.files) {
            var files = event.originalEvent.dataTransfer.files
            if (files.length > 1) {
                var names = []
                for (var i = 0; i < files.length; i++) {
                    names.push(files[i].name)
                }
                $('#title').text(names.join(', '))
            }
            else {
                $('#title').text(files[0].name)
            }
            seed(files)
        }
        return false
    })
        .on('dragover', function (event) {
            event.preventDefault()
            event.stopPropagation()
            holder.addClass('white-border')
            return false
        })
        .on('dragleave', function (event) {
            event.preventDefault()
            event.stopPropagation()
            holder.removeClass('white-border')
            return false
        })
    if (!window.FileReader || !WebTorrent.WEBRTC_SUPPORT) {
        $('#status').text('WebTorrent NOT Ready')
    } else {
        $('#status').text('WebTorrent Ready')
        $('#holder').text('Drag&Drop files here to share')
        if (window.location.hash.substr(1)) {
            var client = new WebTorrent({
                    tracker: WebTorrent.WEBRTC_SUPPORT ? {
                        rtcConfig: {
                            iceServers: iceServers
                        }
                    } : false
                }),
                torrent = client.add(window.location.hash.substr(1), {
                announce: trackers
            })
            torrentEvents(torrent)
        }
    }
})
